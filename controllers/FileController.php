<?php

namespace app\controllers;

use Yii;
use app\models\File;
use app\models\FileSearch;
use app\forms\UploadForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * FileController
 */
class FileController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'upload' => ['POST'],
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all File models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'form' => new UploadForm(),
        ]);
    }

    /**
     * Upload file
     * @return \yii\web\Response
     */
    public function actionUpload()
    {
        $form = new UploadForm();
        $form->file = UploadedFile::getInstance($form, 'file');

        $filename = md5(microtime(1)) . '.xml';
        if ($form->validate() && $form->file->saveAs('files/' . $filename)) {
            File::create($filename, $form->file->baseName . '.' . $form->file->extension);
            Yii::$app->session->setFlash('success', 'Upload success!');
        } else {
            $errors = implode('; ', $form->getErrorSummary(true));
            Yii::$app->session->setFlash('error', 'Upload error! ' . $errors);
        }

        return $this->redirect(['index']);
    }

    /**
     * Displays a single File model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }




    /**
     * Deletes an existing File model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $file = $this->findModel($id);
        unlink(Yii::$app->basePath . '/web/files/' . $file->name);
        $file->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the File model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return File the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = File::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
