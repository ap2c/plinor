<?php

namespace app\controllers\api;

use app\models\File;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;

class FileController extends Controller
{

    /**
     * @return array
     */
    public function actionList()
    {
        $files = array_map(function ($file) {
            return [
                'id' => $file->id,
                'name' => $file->name,
                'upload_at' => $file->upload_at,
            ];
        }, File::find()->limit(100)->all());

        return $files;
    }

    /**
     * @param $ids
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionView($ids)
    {
        if (($files = File::find()->where(['id' => explode(',', $ids)])->all()) === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $result = [];
        foreach ($files as $file) {
            $path = \Yii::$app->basePath . '/web/files/' . $file->name;
            $xml = simplexml_load_file($path);
            $xml->filename = $file->original;
            $result[] = $xml;
        }
        return $result;
    }

}
