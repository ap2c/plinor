<?php

namespace app\components\validators;

use yii\validators\Validator;

class PatternFileValidator extends Validator
{

    public function validateAttribute($form, $attribute)
    {
        if (!is_file($form->$attribute->tempName)) {
            $this->addError($form, $attribute, 'Файл не загружен.');
            return;
        }

        try {
            $xml = simplexml_load_file($form->$attribute->tempName);
        } catch (\yii\base\ErrorException $e) {
            $this->addError($form, $attribute, 'Файл имеет не верную структуру xml.');
            return;
        }

        $component = $xml->xpath('//Component[@Id="030-032-000-000"]');

        if (empty($component[0])
            || !empty($component[0]->Value)
            || !empty($component[0]->Limit)
            || 'ERROR' != $component[0]->Error
        ) {
            $this->addError($form, $attribute, 'Файл не соответствует разрешенному шаблону.');
        }
    }
}