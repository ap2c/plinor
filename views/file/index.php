<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Files';
?>
<div class="file-index">

    <div id="upload-form" class="form-inline pull-right">
        <?php $activeForm = ActiveForm::begin(['action' => '/file/upload']) ?>

        <?= $activeForm->field($form, 'file')->label(false)->fileInput(['class' => 'form-control-file']) ?>

        <button class="btn btn-success">Submit</button>

        <?php ActiveForm::end() ?>
    </div>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'original:text:Original name',
            'upload_at:text:Uploaded_at',
            ['class' => 'yii\grid\ActionColumn', 'template'=>'{view} &nbsp; {delete}'],
        ],
    ]); ?>


</div>
