<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "file".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $original
 * @property string|null $upload_at
 */
class File extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'file';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['upload_at'], 'safe'],
            [['name'], 'string', 'max' => 64],
            [['original'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'original' => 'Original',
            'upload_at' => 'Upload At',
        ];
    }

    /**
     * Create File
     * @param string $filename
     * @param string $original
     * @return File
     */
    public static function create(string $filename, string $original): File
    {
        $file = new self([
            'name' => $filename,
            'original' => $original,
            'upload_at' => date('Y-m-d H:i:s'),
        ]);
        $file->save();
        return $file;
    }
}
