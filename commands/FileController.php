<?php

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;

class FileController extends Controller
{

    public function actionIndex()
    {
        return ExitCode::OK;
    }
}
